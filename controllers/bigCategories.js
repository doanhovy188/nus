const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const BigCategory = require('../models/BigCategory')

// @desc    Get big categories
// @route   GET /api/v1/bigCategories
// @access  Private/Admin
exports.getBigCategories = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults)
})

// @desc    Get single big category
// @route   GET /api/v1/bigCategories/:id
// @access  Private/Admin
exports.getBigCategory = asyncHandler(async (req, res, next) => {
  const bigCategory = await BigCategory.findById(req.params.id)
    .populate({ path: 'miniCategories' , select: '_id name'});

  if (!bigCategory) {
    return next(
      new ErrorResponse(`No big Category with that id of ${req.params.id}`)
    )
  }

  res.status(200).json({ sucess: true, data: bigCategory })
})