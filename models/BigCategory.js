const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const Schema = mongoose.Schema

const BigCategorySchema = new Schema(
  {
    name: {
      type: String,
      minlength: [3, 'Name must be three characters long'],
      trim: true,
      unique: true,
      uniqueCaseInsensitive: true,
      required: [true, 'Name is required'],
    },
  },
  { timestamps: true }
)

BigCategorySchema.virtual('miniCategories', {
  ref: 'MiniCategory',
  localField: '_id',
  foreignField: 'miniCategoryId',
  justOne: false,
})

BigCategorySchema.plugin(uniqueValidator, { message: '{PATH} already exists.' })

module.exports = mongoose.model('BigCategory', BigCategorySchema)
