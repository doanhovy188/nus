const express = require('express')
const {
  getHashTags,
  getHashtag,
  addHashtag,
  deleteHashtag
} = require('../controllers/hashtags')

const Hashtag = require('../models/Hashtag')

const router = express.Router()

const advancedResults = require('../middleware/advancedResults')
const { protect, authorize } = require('../middleware/auth')



module.exports = router
