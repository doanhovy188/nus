const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')
const Hashtag = require('../models/Hashtag')

// @desc    Get hashtags
// @route   GET /api/v1/hashtag
// @access  Private/Admin
exports.getHashtags = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults)
})

// @desc    Get single hashtag
// @route   GET /api/v1/hashtags/:id
// @access  Private/Admin
exports.getHashtags = asyncHandler(async (req, res, next) => {
  const hashtag = await Hashtag.findById(req.params.id)
    .populate({ path: 'postId' });

  if (!hashtag) {
    return next(
      new ErrorResponse(`No hashtag with that id of ${req.params.id}`)
    )
  }

  res.status(200).json({ sucess: true, data: hashtag })
})