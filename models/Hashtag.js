const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const Schema = mongoose.Schema

const HashtagSchema = new Schema(
  {
    name: {
      type: String,
      minlength: [3, 'Name must be three characters long'],
      trim: true,
      unique: true,
      uniqueCaseInsensitive: true,
      required: [true, 'Name is required'],
    },
    postId: [
      { 
        type: Schema.ObjectId,
        ref: "Post"
      }
    ],
  },
  { timestamps: true }
)

HashtagSchema.plugin(uniqueValidator, { message: '{PATH} already exists.' })

module.exports = mongoose.model('Hashtag', HashtagSchema)
