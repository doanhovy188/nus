const express = require('express')
const {
  getBigCategories,
  getBigCategory,
  addBigCategory,
  updateBigCategory,
  deleteBigCategory
} = require('../controllers/bigCategories')

const BigCategories = require('../models/BigCategory')

const router = express.Router()

const advancedResults = require('../middleware/advancedResults')
const { protect, authorize } = require('../middleware/auth')



module.exports = router
