const express = require("express");
const app = express();

const path = require('path');
const dotenv = require("dotenv");
const cookieParser = require("cookie-parser");
const morgan = require('morgan');
const cors = require('cors');

const errorHandler = require('./middleware/error')

const DBConnection = require('./config/db')

dotenv.config({ path: './config/.env' })

DBConnection()

const authRoutes = require('./routes/auth')
const userRoutes = require('./routes/users')
const miniCategoryRoutes = require('./routes/miniCategories')
const bigCategoryRoutes = require('./routes/bigCategories')
const hashtagRoutes = require('./routes/hashtags')
const postRoutes = require('./routes/posts')
const searchRoutes = require('./routes/search')


app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.use(cookieParser());

// Enable CORS
app.use(cors({
  origin: true, //included origin as true
  credentials: true, //included credentials as true
}))

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

app.use(express.static(path.join(__dirname, 'public')))

const versionOne = (routeName) => `/api/v1/${routeName}`

app.use(versionOne('auth'), authRoutes)
app.use(versionOne('users'), userRoutes)
app.use(versionOne('miniCategories'), miniCategoryRoutes)
app.use(versionOne('bigCategories'), bigCategoryRoutes)
app.use(versionOne('hashtags'), hashtagRoutes)
app.use(versionOne('posts'), postRoutes)
app.use(versionOne('search'), searchRoutes)

app.use(errorHandler)

const PORT = process.env.PORT

const server = app.listen(PORT, () => {
  console.log(
    `We are live on ${process.env.NODE_ENV} mode on port ${PORT}`
  )
})

// Handle unhandled promise rejections
process.on('unhandledRejection', (err, promise) => {
  console.log(`Error: ${err.message}`.red)
  // Close server & exit process
  server.close(() => process.exit(1))
})