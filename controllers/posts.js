const asyncHandler = require('../middleware/async')
const ErrorResponse = require('../utils/errorResponse')

const Post = require('../models/Post')

// @desc    Get posts
// @route   GET /api/v1/posts/public or /api/v1/posts/private
// @access  Public Or Private
exports.getPosts = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults)
})

// @desc    Get single post
// @route   GET /api/v1/posts/:id
// @access  Public
exports.getPost = asyncHandler(async (req, res, next) => {
  const post = await Post.findById(req.params.id)
    .populate({
      path: 'hashtagId'
    })
    .populate({ path: 'userId', select: 'name imageUrl' })

  if (!post) {
    return next(new ErrorResponse(`No post with that id of ${req.params.id}`))
  }

  res.status(200).json({ sucess: true, data: post })
})